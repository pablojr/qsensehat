CONFIG += c++11

SOURCES = main.cpp

LIBS += -L/usr/local/lib -lQSenseEmu

INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/lib
