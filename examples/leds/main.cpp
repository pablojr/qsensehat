/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include <QCoreApplication>
#include <QTimer>
#include "qsensehatledmatrix.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QSenseHATLedMatrix screen;

    screen.clear(Qt::lightGray);
    screen.setPixel(2, 5, QColor(Qt::red));
    screen.setPixel(3, 6, QColor(230, 240, 250));

    QColor color = screen.getPixel(3, 6);
    qDebug() << "Pixel color: R" << color.red() << "G" << color.green() << "B" << color.blue();

    QTimer::singleShot(3000, &app, &QCoreApplication::quit);

    return app.exec();
}
