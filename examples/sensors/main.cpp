/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include <QCoreApplication>
#include <QTimer>
#include "qsensehatsensors.h"

int main(int argc, char *argv[])
{
    QLoggingCategory::setFilterRules(QStringLiteral("qsensehat.sensors=true"));
    QCoreApplication app(argc, argv);

    QSenseHATSensors sensors;
    sensors.setAutoPoll(true);

    QObject::connect(&sensors, &QSenseHATSensors::humidityChanged, [](qreal h) {
        qDebug() << "Humidity:" << h;
    });
    QObject::connect(&sensors, &QSenseHATSensors::pressureChanged, [](qreal p) {
        qDebug() << "Pressure:" << p;
    });
    QObject::connect(&sensors, &QSenseHATSensors::temperatureChanged, [](qreal c) {
        qDebug() << "Temperature:" << c;
    });
    QObject::connect(&sensors, &QSenseHATSensors::gyroChanged, [](const QVector3D &v) {
        qDebug() << "Gyro:" << v.x() << v.y() << v.z();
    });
    QObject::connect(&sensors, &QSenseHATSensors::accelerationChanged, [](const QVector3D &v) {
        qDebug() << "Acceleration:" << v.x() << v.y() << v.z();
    });
    QObject::connect(&sensors, &QSenseHATSensors::compassChanged, [](const QVector3D &v) {
        qDebug() << "Compass:" << v.x() << v.y() << v.z();
    });
    QObject::connect(&sensors, &QSenseHATSensors::orientationChanged, [](const QVector3D &v) {
        qDebug() << "Orientation:" << v.x() << v.y() << v.z();
    });

    QTimer::singleShot(20000, &app, &QCoreApplication::quit);

    return app.exec();
}
