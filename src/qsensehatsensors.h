/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef QSENSEHATSENSORS_H
#define QSENSEHATSENSORS_H

#include <QObject>
#include <QLoggingCategory>
#include "qsensehatglobal.h"
#include <QVector3D>

Q_DECLARE_LOGGING_CATEGORY(qLcSHSensors)

class QSenseHATSensorsPrivate;

class QSENSEHAT_EXPORT QSenseHATSensors : public QObject
{
    Q_OBJECT

public:
    enum UpdateFlag {
        UpdateHumidity = 0x01,
        UpdatePressure = 0x02,
        UpdateTemperature = 0x04,
        UpdateGyro = 0x08,
        UpdateAcceleration = 0x10,
        UpdateCompass = 0x20,
        UpdateOrientation = 0x40,
        UpdateAll = 0xFF
    };
    Q_DECLARE_FLAGS(UpdateFlags, UpdateFlag)

    QSenseHATSensors();
    ~QSenseHATSensors();

    void poll(UpdateFlags what = UpdateAll);
    void setAutoPoll(bool enable, UpdateFlags what = UpdateAll);
    bool isTemperatureFromPressure() const;
    void setTemperatureFromPressure(bool enable);
    void setPollInterval(int interval);

    qreal humidity() const;
    qreal pressure() const;
    qreal temperature() const;
    QVector3D gyro() const;
    QVector3D acceleration() const;
    QVector3D compass() const;
    QVector3D orientation() const;

signals:
    void humidityChanged(qreal value);
    void pressureChanged(qreal value);
    void temperatureChanged(qreal value);
    void gyroChanged(const QVector3D &value);
    void accelerationChanged(const QVector3D &value);
    void compassChanged(const QVector3D &value);
    void orientationChanged(const QVector3D &value);

private:
    Q_DISABLE_COPY(QSenseHATSensors)
    Q_DECLARE_PRIVATE(QSenseHATSensors)

    QSenseHATSensorsPrivate *d_ptr;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QSenseHATSensors::UpdateFlags)

#endif // QSENSEHATSENSORS_H
