/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef QSENSEHATSTICK_H
#define QSENSEHATSTICK_H

#include <QObject>
#include <QLoggingCategory>
#include "qsensehatglobal.h"

Q_DECLARE_LOGGING_CATEGORY(qLcSHStick)

class QSenseHATStickPrivate;

class QSENSEHAT_EXPORT QSenseHATStick : public QObject
{
    Q_OBJECT

public:
    enum Direction {
        Up = 0x01,
        Down = 0x02,
        Left = 0x04,
        Right = 0x08,
        Middle = 0x10
    };
    Q_DECLARE_FLAGS(Directions, Direction)

    enum Action {
        Pressed = 0x01,
        Released = 0x02,
        Held = 0x04
    };
    Q_DECLARE_FLAGS(Actions, Action)

    QSenseHATStick();
    ~QSenseHATStick();

    bool isReady() const;
    void init();

signals:
    void moved();

private:
    Q_DISABLE_COPY(QSenseHATStick)
    Q_DECLARE_PRIVATE(QSenseHATStick)

    QSenseHATStickPrivate *d_ptr;
};

#endif // QSENSEHATSTICK_H
