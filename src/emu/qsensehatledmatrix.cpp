/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "qsensehatledmatrix.h"
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QDataStream>

Q_LOGGING_CATEGORY(qLcSHLedMatrix, "qsensehat.ledmatrix")

class QSenseHATLedMatrixPrivate
{

public:
    QSenseHATLedMatrixPrivate(QSenseHATLedMatrix *q_ptr);
    ~QSenseHATLedMatrixPrivate();

    const int MAX_X_SIZE = 8;
    const int MAX_Y_SIZE = 8;

    QSenseHATLedMatrix *q;
    void open();
    bool clearMatrix(const QColor color = Qt::black);
    bool setPixel(int x, int y, QColor color);
    void setLowLight(bool enable);
    QColor getPixel(int x, int y);

    QFile fdScreen;
    bool _valid;
    QString _lastError;
    QSize _size;

private:
    int locationFromCoordinates(int x, int y);
    unsigned char highByteFromColor(QColor color);
    unsigned char lowByteFromColor(QColor color);
    bool isValidCoordinates(int x, int y);
};

QSenseHATLedMatrixPrivate::QSenseHATLedMatrixPrivate(QSenseHATLedMatrix *q_ptr)
    : q(q_ptr)
{
    _valid = false;
    _size = QSize(MAX_X_SIZE, MAX_Y_SIZE);
}

QSenseHATLedMatrixPrivate::~QSenseHATLedMatrixPrivate()
{

}

int QSenseHATLedMatrixPrivate::locationFromCoordinates(int x, int y)
{
    return ((y * MAX_Y_SIZE + x) * 2);
}

void QSenseHATLedMatrixPrivate::open()
{
    #define message "Cannot find proper folder from sense-emu"

#ifdef Q_OS_WIN
    if (!QDir::setCurrent(QDir::tempPath())) {
        qCritical(message);
        return;
    }
#else
    if (!QDir::setCurrent(QStringLiteral("/dev/shm"))) {
        if (!QDir::setCurrent(QStringLiteral("/tmp"))) {
            qCritical(message);
            return;
        }
    }
#endif

    fdScreen.setFileName(QStringLiteral("rpi-sense-emu-screen"));
    if (!fdScreen.open(QIODevice::ReadWrite)) {
        qCritical("Cannot open screen data file");
        return;
    }
    _valid = true;
}

unsigned char QSenseHATLedMatrixPrivate::highByteFromColor(QColor color)
{
    return ((color.red() & 0xF8) | (color.green() >> 5));
}

unsigned char QSenseHATLedMatrixPrivate::lowByteFromColor(QColor color)
{
    return (((color.green() & 0x1C) << 3) | (color.blue()  >> 3));
}

bool QSenseHATLedMatrixPrivate::clearMatrix(const QColor color)
{
    QDataStream stream(&fdScreen);
    fdScreen.reset();
    for (int i = 0; i < MAX_X_SIZE * MAX_Y_SIZE; i++) {
        stream << lowByteFromColor(color) << highByteFromColor(color);
    }
    return (stream.status() == QDataStream::Ok);
}

bool QSenseHATLedMatrixPrivate::isValidCoordinates(int x, int y)
{
    bool xValid = false;
    bool yValid = false;

    if (!_valid)
    {
        _lastError = QStringLiteral("Device not initialized!");
        return false;
    }

    xValid = (x >= 0 && x < MAX_X_SIZE);
    yValid = (y >= 0 && y < MAX_Y_SIZE);
    if ( !xValid || !yValid ) {
        if (!xValid && yValid ) {
            _lastError = QStringLiteral("Invalid X coordinate");
        } else if (xValid && !yValid) {
            _lastError = QStringLiteral("Invalid Y coordinate");
        } else {
            _lastError = QStringLiteral("Invalid X and Y coordinates");
        }
        return false;
    }
    return true;
}

QColor QSenseHATLedMatrixPrivate::getPixel(int x, int y)
{
    QColor color = nullptr;
    if (isValidCoordinates(x, y)) {
        fdScreen.reset();
        fdScreen.seek(locationFromCoordinates(x, y));
        QByteArray bytes;
        bytes = fdScreen.read(2);
        quint16 colorValue = (quint8)bytes.at(0) + (quint8)bytes.at(1) * 256;
        color.setRed(((colorValue & 0xF800) >> 11) << 3);
        color.setGreen(((colorValue & 0x7E0) >> 5) << 2);
        color.setBlue((colorValue & 0x1F) << 3);
    }
    return color;
}

bool QSenseHATLedMatrixPrivate::setPixel(int x, int y, QColor color)
{
    bool status = false;
    if (isValidCoordinates(x, y)) {
        QDataStream stream(&fdScreen);
        fdScreen.reset();
        fdScreen.seek(locationFromCoordinates(x, y));
        // Emulator uses RGB565 color palette (5 bits for red, 6 for green, 5 for blue)
        // rrrrrggg|gggbbbbb
        stream << lowByteFromColor(color) << highByteFromColor(color);
        status = stream.status();
    }
    return status;
}

/*
 * begins implementation of "exposed" class
 */

QSenseHATLedMatrix::QSenseHATLedMatrix()
    : d_ptr(new QSenseHATLedMatrixPrivate(this))
{
    d_ptr->open();
}

QSenseHATLedMatrix::~QSenseHATLedMatrix()
{
    delete d_ptr;
}

bool QSenseHATLedMatrix::isValid() const
{
    Q_D(const QSenseHATLedMatrix);
    return d->_valid;
}

QSize QSenseHATLedMatrix::size() const
{
    Q_D(const QSenseHATLedMatrix);
    return d->_size;
}

/**
 * @brief Sets the entire LED matrix to a single color, defaults to blank / off.
 * @return true if successful, false otherwise
 */
bool QSenseHATLedMatrix::clear(QColor color)
{
    Q_D(QSenseHATLedMatrix);
    return d->clearMatrix(color);
}

/**
 * @brief Sets the pixel value for the {x,y} location on the LED matrix
 * @param x the X axis position (0 based)
 * @param y the Y axis position (0 based)
 * @param color A QColor (as RGB) value
 * @return true if successful, false otherwise
 */
bool QSenseHATLedMatrix::setPixel(int x, int y, QColor color)
{
    Q_D(QSenseHATLedMatrix);
    return d->setPixel(x, y, color);
}

/**
 * @brief Gets the color value of pixel at {x,y} location on the LED matrix
 * @param x the X axis position (0 based)
 * @param y the Y axis position (0 based)
 * @return A QColor (from RGB565), may not be original value
 */
QColor QSenseHATLedMatrix::getPixel(int x, int y)
{
    Q_D(QSenseHATLedMatrix);
    return d->getPixel(x, y);
}
