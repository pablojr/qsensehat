/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "qsensehatstick.h"
#include <QLocalSocket>
#include <QDataStream>
#include <QDebug>

Q_LOGGING_CATEGORY(qLcSHStick, "qsensehat.stick")

class QSenseHATStickPrivate
{

public:
    QSenseHATStickPrivate(QSenseHATStick *q_ptr);
    ~QSenseHATStickPrivate();

    QSenseHATStick *q;
    void open();
    void readStick();
    void handleError(QLocalSocket::LocalSocketError socketError);

    QLocalSocket* socket;
    QDataStream in;
    quint32 blockSize;
    bool ready;
};

QSenseHATStickPrivate::QSenseHATStickPrivate(QSenseHATStick *q_ptr)
    : q(q_ptr)
{
    socket = new QLocalSocket(q);
}

QSenseHATStickPrivate::~QSenseHATStickPrivate()
{

}

void QSenseHATStickPrivate::open()
{
    socket->connectToServer("/dev/shm/rpi-sense-emu-stick");
}

void QSenseHATStickPrivate::readStick() {
    ready = true;
    if (blockSize == 0) {
        // Relies on the fact that QDataStream serializes a quint32
        // into sizeof(quint32) bytes
        if (socket->bytesAvailable() < (int)sizeof(quint32))
            return;
        in >> blockSize;
    }

    if (socket->bytesAvailable() < blockSize || in.atEnd())
        return;

    QString event;
    in >> event;
    qDebug() << event;

    emit q->moved();
}

void QSenseHATStickPrivate::handleError(QLocalSocket::LocalSocketError socketError) {
    qDebug() << "Error " << socketError;
    ready = false;
}

/*
 * begins implementation of "exposed" class
 */

QSenseHATStick::QSenseHATStick()
    : d_ptr(new QSenseHATStickPrivate(this))
{
    connect(d_ptr->socket, &QLocalSocket::readyRead, [this] { d_ptr->readStick(); });
//    connect(d_ptr->socket, QOverload<QLocalSocket::LocalSocketError>::of(&QLocalSocket::error),
//            [this] {d_ptr->handleError(); ) });
}

QSenseHATStick::~QSenseHATStick()
{
    delete d_ptr;
}

void QSenseHATStick::init()
{
    d_ptr->open();
}

bool QSenseHATStick::isReady() const
{
    Q_D(const QSenseHATStick);
    return d->ready;
}
