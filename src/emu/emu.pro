TARGET = QSenseEmu

TEMPLATE = lib

DEFINES += QSENSEHAT_BUILD_LIB

QT += core gui network

CONFIG += c++11

INCLUDEPATH += ../

SOURCES += \
    qsensehatstick.cpp \
    qsensehatsensors.cpp \
    qsensehatledmatrix.cpp

HEADERS += \
    ../qsensehatstick.h \
    ../qsensehatsensors.h \
    ../qsensehatglobal.h \
    ../qsensehatledmatrix.h

VERSION = 0.1.0

unix {
    target.path = /usr/local/lib
    INSTALLS += target

    iTarget.path = /usr/local/include
    iTarget.files = $$HEADERS
    INSTALLS += iTarget
}
