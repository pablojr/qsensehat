/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "qsensehatsensors.h"
#include <QFile>
#include <QTimer>
#include <QVector3D>
#include <QDir>
#include <QtMath>

#define PRESSURE_FACTOR 4096
#define TEMPERATURE_FACTOR_FROM_PRESSURE 480
#define HUMIDITY_FACTOR 256
#define TEMPERATURE_FACTOR_FROM_HUMIDITY 64
#define ACCEL_FACTOR 4081.6327
#define GYRO_FACTOR 57.142857
#define COMPASS_FACTOR 7142.8571
#define ORIENT_FACTOR 5214.1892

Q_LOGGING_CATEGORY(qLcSHSensors, "qsensehat.sensors")

class QSenseHATSensorsPrivate
{

public:
    QSenseHATSensorsPrivate(QSenseHATSensors *q_ptr);
    ~QSenseHATSensorsPrivate();

    typedef struct
    {
        bool pressureValid;
        float pressure;
        bool temperatureValid;
        float temperature;
        bool humidityValid;
        float humidity;
        QVector3D accel;
        bool accelValid;
        QVector3D gyro;
        bool gyroValid;
        QVector3D compass;
        bool compassValid;
        QVector3D orientation;
        bool orientationValid;
    } EMU_DATA;

    QSenseHATSensors *q;
    void open();
    void update(QSenseHATSensors::UpdateFlags what);
    void report(const EMU_DATA &data, QSenseHATSensors::UpdateFlags what);

    bool humidityRead(EMU_DATA& data);
    bool pressureRead(EMU_DATA& data);
    bool imuRead(EMU_DATA& data);

    int pollInterval = 200;
    QTimer pollTimer;
    QSenseHATSensors::UpdateFlags autoPollWhat;
    bool temperatureFromPressure = false;
    QFile fdImu;
    QFile fdHumidity;
    QFile fdPressure;

    qreal _humidity = 0;
    qreal _pressure = 0;
    qreal _temperature = 0;
    QVector3D _gyro;
    QVector3D _acceleration;
    QVector3D _compass;
    QVector3D _orientation;
    bool _ready;
};

QSenseHATSensorsPrivate::QSenseHATSensorsPrivate(QSenseHATSensors *q_ptr)
    : q(q_ptr)
{

}

static inline qreal toDeg360(qreal rad)
{
    const qreal deg = qRadiansToDegrees(rad);
    return deg < 0 ? deg + 360 : deg;
}

QSenseHATSensorsPrivate::~QSenseHATSensorsPrivate()
{

}

void QSenseHATSensorsPrivate::open()
{
    #define message "Cannot find proper folder from sense-emu"

#ifdef Q_OS_WIN
    if (!QDir::setCurrent(QDir::tempPath())) {
        qCritical(message);
        return;
    }
#else
    if (!QDir::setCurrent(QStringLiteral("/dev/shm"))) {
        if (!QDir::setCurrent(QStringLiteral("/tmp"))) {
            qCritical(message);
            return;
        }
    }
#endif

    fdImu.setFileName(QStringLiteral("rpi-sense-emu-imu"));
    if (!fdImu.open(QIODevice::ReadOnly)) {
        qCritical("Cannot open IMU sensor data file");
        return;
    }

    fdHumidity.setFileName(QStringLiteral("rpi-sense-emu-humidity"));
    if (!fdHumidity.open(QIODevice::ReadOnly)) {
        qCritical("Cannot open humidity sensor data file");
        return;
    }

    fdPressure.setFileName(QStringLiteral("rpi-sense-emu-pressure"));
    if (!fdPressure.open(QIODevice::ReadOnly)) {
        qCritical("Cannot open pressure sensor data file");
        return;
    }
}

bool QSenseHATSensorsPrivate::humidityRead(EMU_DATA& data)
{
    QByteArray bytes;
    data.humidityValid = false;
    if (fdHumidity.isOpen() && fdHumidity.reset()) {
        bytes = fdHumidity.readAll();
        qCDebug(qLcSHSensors, "Humidity sensor name %s", bytes.mid(2, bytes.at(1)).toStdString().c_str());
        data.humidity = (float)(bytes.at(22) + bytes.at(23) * 256) / HUMIDITY_FACTOR;
        data.humidityValid = (bytes.at(26) == 0x01);
        data.temperature = (float)(bytes.at(24) + bytes.at(25) * 256) / TEMPERATURE_FACTOR_FROM_HUMIDITY;
        data.temperatureValid = (bytes.at(27) == 0x01);
    }
    return data.humidityValid;
}

bool QSenseHATSensorsPrivate::pressureRead(EMU_DATA& data)
{
    QByteArray bytes;
    data.pressureValid = false;
    if (fdPressure.isOpen() && fdPressure.reset()) {
        bytes = fdPressure.readAll();
        qCDebug(qLcSHSensors, "Pressure sensor name %s", bytes.mid(2, bytes.at(1)).toStdString().c_str());
        data.pressure = (float)(bytes.at(16) + bytes.at(17) * 256 + bytes.at(18) * 65536 + bytes.at(19) * 16777216) / PRESSURE_FACTOR;
        data.pressureValid = (bytes.at(26) == 0x01);
        data.temperature = (float)(bytes.at(24) + bytes.at(25) * 256) / TEMPERATURE_FACTOR_FROM_PRESSURE;
        data.temperatureValid = (bytes.at(27) == 0x01);
    }
    return data.pressureValid;
}

bool QSenseHATSensorsPrivate::imuRead(EMU_DATA& data)
{
    QByteArray bytes;
    data.accelValid = true;
    data.gyroValid = true;
    data.compassValid = true;
    data.orientationValid = true;
    if (fdImu.isOpen() && fdImu.reset()) {
        bytes = fdImu.readAll();
        qCDebug(qLcSHSensors, "IMU sensor name %s", bytes.mid(2, bytes.at(1)).toStdString().c_str());
        data.accel.setX((bytes.at(30) + bytes.at(31) * 256) / ACCEL_FACTOR);
        data.accel.setY((bytes.at(32) + bytes.at(33) * 256) / ACCEL_FACTOR);
        data.accel.setZ((bytes.at(34) + bytes.at(35) * 256) / ACCEL_FACTOR);
        data.gyro.setX((bytes.at(36) + bytes.at(37) * 256) / GYRO_FACTOR);
        data.gyro.setY((bytes.at(38) + bytes.at(39) * 256) / GYRO_FACTOR);
        data.gyro.setZ((bytes.at(40) + bytes.at(41) * 256) / GYRO_FACTOR);
        data.compass.setX((bytes.at(42) + bytes.at(43) * 256) / COMPASS_FACTOR);
        data.compass.setY((bytes.at(44) + bytes.at(45) * 256) / COMPASS_FACTOR);
        data.compass.setZ((bytes.at(46) + bytes.at(47) * 256) / COMPASS_FACTOR);
        data.orientation.setX(toDeg360((bytes.at(48) + bytes.at(49) * 256) / ORIENT_FACTOR));
        data.orientation.setY(toDeg360((bytes.at(50) + bytes.at(51) * 256) / ORIENT_FACTOR));
        data.orientation.setZ(toDeg360((bytes.at(52) + bytes.at(53) * 256) / ORIENT_FACTOR));
    }
    return data.accelValid ||
            data.gyroValid ||
            data.compassValid ||
            data.orientationValid;
}

void QSenseHATSensorsPrivate::update(QSenseHATSensors::UpdateFlags what)
{
    int humFlags = QSenseHATSensors::UpdateHumidity;
    if (!temperatureFromPressure)
        humFlags |= QSenseHATSensors::UpdateTemperature;
    if (what & humFlags) {
        EMU_DATA data;
        if (humidityRead(data)) {
            report(data, what & humFlags);
        } else {
            qWarning("Failed to read humidity data");
        }
    }

    int presFlags = QSenseHATSensors::UpdatePressure;
    if (temperatureFromPressure)
        presFlags |= QSenseHATSensors::UpdateTemperature;
    if (what & presFlags) {
        EMU_DATA data;
        if (pressureRead(data))
            report(data, what & presFlags);
        else
            qWarning("Failed to read pressure data");
    }

    const int imuFlags = QSenseHATSensors::UpdateGyro | QSenseHATSensors::UpdateAcceleration
            | QSenseHATSensors::UpdateCompass | QSenseHATSensors::UpdateOrientation;
    if (what & imuFlags) {
        EMU_DATA data;
        if (imuRead(data))
            report(data, what & imuFlags);
        else
            qWarning("Failed to read inertial measurement unit data");
    }
}

void QSenseHATSensorsPrivate::report(const EMU_DATA &data, QSenseHATSensors::UpdateFlags what)
{
    if (what.testFlag(QSenseHATSensors::UpdateHumidity)) {
        if (data.humidityValid) {
            emit q->humidityChanged(data.humidity);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdatePressure)) {
        if (data.pressureValid) {
            emit q->pressureChanged(data.pressure);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdateTemperature)) {
        if (data.temperatureValid) {
            emit q->temperatureChanged(data.temperature);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdateGyro)) {
        if (data.gyroValid) {
            emit q->gyroChanged(data.gyro);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdateAcceleration)) {
        if (data.accelValid) {
            emit q->accelerationChanged(data.accel);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdateCompass)) {
        if (data.compassValid) {
            emit q->compassChanged(data.compass);
        }
    }

    if (what.testFlag(QSenseHATSensors::UpdateOrientation)) {
        if (data.orientationValid) {
            emit q->orientationChanged(data.orientation);
        }
    }
}

/*
 * begins implementation of "exposed" class
 */

QSenseHATSensors::QSenseHATSensors()
    : d_ptr(new QSenseHATSensorsPrivate(this))
{
    d_ptr->open();
    d_ptr->pollTimer.setInterval(d_ptr->pollInterval);
    connect(&d_ptr->pollTimer, &QTimer::timeout, [this] { d_ptr->update(d_ptr->autoPollWhat); });
}

QSenseHATSensors::~QSenseHATSensors()
{
    delete d_ptr;
}

void QSenseHATSensors::setPollInterval(int interval)
{
    Q_D(QSenseHATSensors);
    d->pollInterval = interval;
}

void QSenseHATSensors::poll(UpdateFlags what)
{
    Q_D(QSenseHATSensors);
    d->update(what);
}

void QSenseHATSensors::setAutoPoll(bool enable, UpdateFlags what)
{
    Q_D(QSenseHATSensors);
    if (enable) {
        d->autoPollWhat = what;
        d->pollTimer.start();
    } else {
        d->pollTimer.stop();
    }
}

bool QSenseHATSensors::isTemperatureFromPressure() const
{
    Q_D(const QSenseHATSensors);
    return d->temperatureFromPressure;
}

void QSenseHATSensors::setTemperatureFromPressure(bool enable)
{
    Q_D(QSenseHATSensors);
    d->temperatureFromPressure = enable;
}

qreal QSenseHATSensors::humidity() const
{
    Q_D(const QSenseHATSensors);
    return d->_humidity;
}

qreal QSenseHATSensors::pressure() const
{
    Q_D(const QSenseHATSensors);
    return d->_pressure;
}

qreal QSenseHATSensors::temperature() const
{
    Q_D(const QSenseHATSensors);
    return d->_temperature;
}

QVector3D QSenseHATSensors::gyro() const
{
    Q_D(const QSenseHATSensors);
    return d->_gyro;
}

QVector3D QSenseHATSensors::acceleration() const
{
    Q_D(const QSenseHATSensors);
    return d->_acceleration;
}

QVector3D QSenseHATSensors::compass() const
{
    Q_D(const QSenseHATSensors);
    return d->_compass;
}

QVector3D QSenseHATSensors::orientation() const
{
    Q_D(const QSenseHATSensors);
    return d->_orientation;
}
