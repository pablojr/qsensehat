/****************************************************************************
**
** Copyright (C) 2018 Pablo J. Rogina
** Contact: https://bitbucket.org/pablojr/qsensehat
**
** This file is part of the Sense HAT library for Qt.
**
** Based on ideas and code from projects:
** QSenseHat                    https://github.com/stcote/QSenseHat
** Qt module from Qt Labs       https://github.com/qt-labs/qtsensehat
**
** Sense HAT library for Qt is free software: you can redistribute it and/or
** modify it under the terms of the GNU General Public License as published
** by the Free Software Foundation, either version 3 of the License, or (at
** your option) any later version.
**
** Sense HAT library for Qt is distributed in the hope that it will be
** useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
** Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with Sense HAT library for Qt.
** If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef QSENSEHATLEDMATRIX_H
#define QSENSEHATLEDMATRIX_H

#include <QObject>
#include <QLoggingCategory>
#include <QSize>
#include <QColor>
#include "qsensehatglobal.h"

Q_DECLARE_LOGGING_CATEGORY(qLcSHLedMatrix)

class QImage;
class QSenseHATLedMatrixPrivate;

class QSENSEHAT_EXPORT QSenseHATLedMatrix : public QObject
{
    Q_OBJECT

public:
    QSenseHATLedMatrix();
    ~QSenseHATLedMatrix();

    QSize size() const;
    bool isValid() const;
    void setLowLight(bool enable);
    bool clear(QColor color = Qt::black);
    bool setPixel(int x, int y, QColor color);
    QColor getPixel(int x, int y);
    QImage *paintDevice();

private:
    Q_DISABLE_COPY(QSenseHATLedMatrix)
    Q_DECLARE_PRIVATE(QSenseHATLedMatrix)

    QSenseHATLedMatrixPrivate *d_ptr;
};

#endif
