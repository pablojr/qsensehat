# QSenseHAT #

A Qt-based library to access the [Sense HAT](https://www.raspberrypi.org/products/sense-hat/) add-on board for Raspberry Pi.

It can be built in two different ways: to access the real hardware or to access the [Sense HAT Emulator](https://github.com/RPi-Distro/python-sense-emu).

The hardware mode requires RTIMULib to be installed in the build environment, and it�s usually intended to be built in a cross-compile setup, since the resulting library will be ultimately used in a real Raspberry Pi board.

The emulator mode is not bound to RTIMULib since it's usually intended to be built in the same environment where a Sense HAT emulator runs.

The library is based on ideas and code from [Qt experimental module](https://github.com/qt-labs/qtsensehat) and [QSenseHat library](https://github.com/stcote/QSenseHat) from Steve Cote.

### Status ###

As of this writing the library is only focused on the implementation for "software", i.e. to deal with the Sense Emulator. I don't own an actual board so far, thus preventing me to developing the implementation for "hardware".  

### Examples ###

* Sensors
* LEDs
* Joystick (TODO)
